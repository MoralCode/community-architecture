= Fedora Community Architecture
:doc-fca: https://docs.fedoraproject.org/en-US/council/fca/
:license-image: https://licensebuttons.net/l/by-sa/4.0/88x31.png
:license-legal: https://creativecommons.org/licenses/by-sa/4.0/
:license-shield: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
:toc:

[link={license-legal}]
image::{license-shield}[License: Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)]

_An organizational home to community architecture work in Fedora, managed by the link:{doc-fca}[Fedora Community Architect] (FCA)_.


[[about]]
== About this repository

This repository is used for the following purposes:

* Source content for Fedora Community Architecture documentation (i.e. `docs.fedoraproject.org/en-US/community/`).
* Program management tool and experimental workflow for the link:{doc-fca}[FCA].

This repository was first discussed in https://pagure.io/Fedora-Council/tickets/issue/416[Fedora-Council/tickets#416].
It provides a public-facing interface to track and manage ongoing community architecture work, requests for support from the community, and other responsibilities of the FCA.

//TODO What can people do here? What kinds of requests can they make?

See the https://gitlab.com/fedora/council/community-architecture/-/issues[GitLab issues] for up-to-date information.


[[participate]]
== How to participate

For now, this repository is in a trial phase.
This may change!
The best thing to use it for is to flag a task, community need, or question that needs the attention of the FCA.
Open an issue if there is something you want to share or discuss.


[[legal]]
== Legal

[link={license-legal}]
image::{license-image}[License: Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)]

All content in this repository is shared under the {license-legal}[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license].
