= CFP reviewer guidelines
Justin W. Flory

This page provides guidance to CFP reviewers for the https://flocktofedora.org[Flock to Fedora] conference.
CFP reviewers are invited by core Flock organizers as volunteers to help curate diverse content and programming at our contributor conference.


[[voting]]
== Voting

The CFP system allows a reviewer to cast a vote between -3 and 3, where -3 means definitely not to accept and a 3 is a perfect submission.
We ask each reviewer to give each of their points a significant meaning to help standardize the value of the voting system.

[[voting-understandable]]
=== Q1: Is the proposal understandable, clear, and written in acceptable English?

* *Give one point (+1)*:
  If abstract is written clearly and the submitter did a good job of describing their topics.
  It should be clear what the submitter will talk about and why their topic(s) are relevant to the Fedora contributor community.
* *Give no point (+0)*:
  They submit an interesting abstract, but there are missing details or uncertainties about what the submitter will prepare.
* *Take one point (-1)*:
  The abstract is poorly written and/or it is unclear what the speaker is proposing.

[[voting-thematic]]
=== Q2: Does the proposal have a clear connection to a Fedora focus area?

* *Give one point (+1)*:
  The proposal has a clear connection to a thematic focus area of the Fedora 2028 Strategy.
  The submitter acknowledges the focus area in their abstract or describes the connection between the focus area and their topic(s) well.
* *Give no point (+0)*:
  The proposal acknowledges a thematic focus area, but the submitter does NOT connect the focus area and their topic(s) well.
* *Take one point (-1)*:
  The proposal does not acknowledge a thematic focus area or seems irrelevant to the focus area selected by the submitter.

As a reminder, the Fedora 2028 Strategy focus areas are below:

* Accessibility (a11y)
* Reaching the World
* Community Sustainability
* Technology Innovation and Leadership
* Editions, Spins, and Interests
* Ecosystem Connections

[[voting-quality]]
=== Q3: As a CFP reviewer and Fedora contributor, do you believe the proposal advances the quality of Flock and the Fedora community?

This question requires subjectivity.
However, as a member of the CFP reviewer team, your subjectivity is what we are looking for.
This point is left up to you to decide how to assign it.
You can come up with your own criteria or just follow a gut feeling.

* *Give one point (+1)*:
  You believe that this proposal advances the quality of Flock to Fedora and the Fedora community.
* *Give no point (+0)*:
  You are unsure whether this proposal advances the quality of Flock to Fedora and the Fedora community.
* *Take one point (-1)*:
  You believe this proposal reduces the quality of Flock to Fedora and the Fedora community.
