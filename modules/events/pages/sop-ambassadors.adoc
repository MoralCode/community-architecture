include::ROOT:partial$attributes.adoc[]

= Best practices for Fedora Ambassadors

This page documents best practices and standard operating procedures for Fedora Ambassadors on duty at Fedora-supported events.
It does not prescribe a hard requirement of what to do as an Ambassador, but it provides guidelines on how to align in-person interaction with the values of the Fedora community.

_Have your own suggestions to these best practices?_
_Propose your ideas link:https://gitlab.com/fedora/council/community-architecture/-/issues[as a new issue] or by link:https://gitlab.com/fedora/council/community-architecture/-/edit/main/modules/events/pages/sop-ambassadors.adoc?ref_type=heads[opening a Merge Request]._


[[four-foundations]]
== Know and understand the Four Foundations

*TL;DNR*:
When in doubt, remember the Four F's.
Freedom, Friends, Features, First.

The Fedora community is a diverse community of people with a variety of backgrounds, nationalities, languages, and views.
We share common ground but we do not agree on everything as a project all the time.
However, all our actions as a community should be centered on a set of shared values that we agree upon.
Fedora has four key values that define who we are as a community, and we call those key values the _Four Foundations_.

Ambassadors are real, human faces behind our project.
For many people, a Fedora Ambassador at an event represents someone who is familiar with the project and knows how things work.
This means they can share their favorite successes of Fedora and/or their biggest challenges or frustrations with Fedora.
When an Ambassador responds to someone at an event, there is not a fixed script.
Often in conversation, one has to improvise.

When an Ambassador needs to do that real-time improvisation, this is when the Four Foundations are a helpful reminder of how to speak rightly as a representative of our diverse community.
Any response should fit within the bounds of our four key values as a community: _Freedom_, _Friends_, _Features_, _First._
You can read more about the meaning of each Foundation in xref:projects::index.adoc[Fedora's Mission and Foundations].


[[etiquette]]
== Etiquette

As representatives of the Fedora community, an Ambassador has important responsibility in being that representative.
The actions of one Ambassador can have a far-reaching effect, for better and for worse.
These etiquette tips will help you start community interactions off on the right footing.

[[etiquette-team]]
=== Know your team.

Spend time to get to know your fellow Fedorans at the event with you.
Learn about their background and topics of knowledge.
This way, if a conference-goer asks a question that one Ambassador cannot answer, there is always someone else who could help answer a query or solve an issue that someone is having.
Knowing about what skills and knowledge your team is bringing to the table makes you an effective community connector.

[[etiquette-visitors]]
=== Prioritize in-front booth conversations with visitors.

When running a booth or stand, make sure that visitors to the booth have first priority for conversations.
While it is natural to want to catch up with fellow Fedora contributors and Ambassadors, a shift at a booth is service to our community to interact with the Fedora community.
Catching up should happen outside of a booth shift or when there is a pause in booth traffic.
It is important to be respectful of visitors to the booth.
If a conversation behind the booth is not inclusive of unattended booth visitors, put it on pause until after interacting with the visitor.
